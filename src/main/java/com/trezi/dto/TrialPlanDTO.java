package com.trezi.dto;

public class TrialPlanDTO {

    String plan_code;

    public String getPlan_code() {
        return plan_code;
    }

    public void setPlan_code(String plan_code) {
        this.plan_code = plan_code;
    }
}

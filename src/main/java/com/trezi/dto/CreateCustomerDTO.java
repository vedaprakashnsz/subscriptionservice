package com.trezi.dto;

import java.util.Date;

public class CreateCustomerDTO {

    /**
     * {"display_name":"Mr.Bhupendra Kumar",
     * "customer_id":"1388063000000078049",
     * "currency_code":"INR",
     * "currency_symbol":"₹",
     * "status":"active",
     * "company_name":"AEIFORIA CONSTRUCTIONS PRIVATE LIMITED",
     * "unused_credits":0.00,
     * "outstanding_receivable_amount":0.00,
     * "outstanding":0.00,
     * "first_name":"Bhupendra",
     * "last_name":"Kumar",
     * "email":"bhupendra@aeiforiaarchitects.com",
     * "phone":"",
     * "mobile":"",
     * "website":"",
     * "is_gapps_customer":false,
     * "created_time":"2018-06-15T13:58:44+0530",
     * "updated_time":"2019-04-02T17:07:53+0530",
     * "is_portal_invitation_accepted":"",
     * "gst_treatment":"business_gst",
     * "gst_no":"07AALCA1437F1ZW",
     * "place_of_contact":"DL",
     * "payment_terms_label":"Due on Receipt",
     * "payment_terms":0,
     * "created_by":"subscriptions",
     * "has_attachment":false}
     */

    String email;
    String first_name;
    String last_name;
    String gst_treatment;
    String gst_no;
    String company_name;
    String display_name;
    String status;


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getGst_treatment() {
        return gst_treatment;
    }

    public void setGst_treatment(String gst_treatment) {
        this.gst_treatment = gst_treatment;
    }

    public String getGst_no() {
        return gst_no;
    }

    public void setGst_no(String gst_no) {
        this.gst_no = gst_no;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

package com.trezi.dto;

public class TrialSubscriptionDTO {

    String customer_id;
    TrialPlanDTO plan;
    String expires_at;
    String created_at;
    String activated_at;

    public String getExpires_at() {
        return expires_at;
    }

    public void setExpires_at(String expires_at) {
        this.expires_at = expires_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getActivated_at() {
        return activated_at;
    }

    public void setActivated_at(String activated_at) {
        this.activated_at = activated_at;
    }

    CustomerDetailsDTO customer;

    public TrialPlanDTO getPlan() {
        return plan;
    }

    public void setPlan(TrialPlanDTO plan) {
        this.plan = plan;
    }

    public CustomerDetailsDTO getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerDetailsDTO customer) {
        this.customer = customer;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }
}

package com.trezi.controllers;


import com.trezi.utility.RestResponse;
import com.trezi.utility.SendStartMail;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;

@CrossOrigin(origins = {"http://test.trezi.com", "http://localhost:8080", "https://subscriptions.trezi.com"}, maxAge = 6000, allowCredentials = "false")
@RestController
public class ReportsController {

    @Inject
    RestResponse restResponse;

    @RequestMapping(value = "/report", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> getReports(@RequestParam("emailIds") String emailIds) {
        SendStartMail.captureData(emailIds);
        return restResponse.success("Mail will be sent");
    }
}

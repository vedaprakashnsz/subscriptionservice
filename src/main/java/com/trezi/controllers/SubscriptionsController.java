package com.trezi.controllers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.trezi.dto.CreateCustomerAndSubscriptionDTO;
import com.trezi.dto.CreateCustomerDTO;
import com.trezi.dto.CustomerSubscriptionDetailsDTO;
import com.trezi.dto.UpdateCustomerDetails;
import com.trezi.model.Credential;
import com.trezi.services.SendGridEmailService;
import com.trezi.services.SubscriptionsService;
import com.trezi.services.UtilityService;
import com.trezi.utility.EmailTemplateParser;
import com.trezi.utility.RestResponse;
import com.trezi.utility.SendStartMail;
import org.apache.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

@CrossOrigin(origins = {"http://test.trezi.com", "http://localhost:8080"}, maxAge = 6000, allowCredentials = "false")
@RestController
public class SubscriptionsController {

    @Inject
    RestResponse restResponse;

    @Inject
    UtilityService utilityService;

    @Inject
    SubscriptionsService subscriptionsService;



    /*@RequestMapping(value = "/products", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> getReports() {
        utilityService.makeGETAPICall("https://subscriptions.zoho.com/api/v1/products", null);
        return restResponse.success("Mail will be sent");
    }*/

    /*@RequestMapping(value = "/user", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> getUser(@RequestParam("email") String email) {
//        utilityService.createUser("https://subscriptions.zoho.com/api/v1/products", null);
        return restResponse.success("Mail will be sent");
    }*/

    @RequestMapping(value = "/customer", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<?> saveItem(@RequestBody CreateCustomerDTO createCustomerDTO) {
        String customerId = subscriptionsService.createUser(createCustomerDTO).toString();
        return restResponse.success("Customer Created " + customerId);
    }

    @RequestMapping(value = "/product", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> getProduct() {
        String productId = subscriptionsService.getProducts();
        return restResponse.success("Product : " + productId);
    }

    @RequestMapping(value = "/plans", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> getPlansForProduct(@RequestParam("productId") String productId) {
        Map plans = subscriptionsService.getPlanForProduct(productId);
        return restResponse.successWithCustomStatus(plans, "Plans", HttpStatus.OK);
    }

    @RequestMapping(value = "/trialSubscription", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> createTrialSubscription(@RequestParam("customerId") String customerId) {
        subscriptionsService.createTrialSubscriptionForExistingCustomer(customerId);
        return restResponse.success("Trial Created");
    }

    @RequestMapping(value = "/onboardTrialSubscription", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<?> onboardTrialCustomer(@RequestBody CreateCustomerDTO createCustomerDTO) {

        String customerId = subscriptionsService.createUser(createCustomerDTO).toString();
        String subscriptionId = subscriptionsService.createTrialSubscriptionForExistingCustomer(customerId);

        CreateCustomerAndSubscriptionDTO createCustomerAndSubscriptionDTO = new CreateCustomerAndSubscriptionDTO();
        createCustomerAndSubscriptionDTO.setCustomerId(customerId);
        createCustomerAndSubscriptionDTO.setSubscriptionId(subscriptionId);

        Map<String, Object> dataMap =
                new ObjectMapper().convertValue(createCustomerAndSubscriptionDTO, new TypeReference<Object>() {});
        return restResponse.successWithCustomStatus(dataMap, "Customer and Subscription Created", HttpStatus.OK);
    }

    @RequestMapping(value = "/details", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> getCustomerDetails(@RequestParam("customerId") String customerId, @RequestParam("subscriptionId") String subscriptionId) {
        CustomerSubscriptionDetailsDTO customerSubscriptionDetailsDTO = subscriptionsService.fetchCustomerAndSubscriptionDetails(customerId, subscriptionId);
        Map<String, Object> dataMap =
                new ObjectMapper().convertValue(customerSubscriptionDetailsDTO, new TypeReference<Object>() {});
        return restResponse.successWithCustomStatus(dataMap, "Customer and Subscription Details", HttpStatus.OK);
    }

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> getUserDetails(@RequestParam("email") String email) {
        CustomerSubscriptionDetailsDTO customerSubscriptionDetailsDTO = subscriptionsService.fetchCustomerByEmail(email);
        Map<String, Object> dataMap =
                new ObjectMapper().convertValue(customerSubscriptionDetailsDTO, new TypeReference<Object>() {});
        return restResponse.successWithCustomStatus(dataMap, "Customer and Subscription Details", HttpStatus.OK);
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> searchUser(@RequestParam("email") String email) {
//        subscriptionsService.updateUser(email);
        return restResponse.success("User searched");
    }

    @RequestMapping(value = "/user", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> updateOrCreateUserDetails(@RequestBody String request) {
        try{
            System.out.println("user details :  " + request);

            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = mapper.readTree(request);
            JsonNode data = root.path("data");
            JsonNode subscription = (JsonNode) data.path("subscription");
            String startDate = subscription.path("current_term_starts_at").asText();
            String endDate = subscription.path("current_term_ends_at").asText();
            String subId = subscription.path("subscription_id").asText();
            JsonNode customer = (JsonNode) subscription.path("customer");
            String custId = customer.path("customer_id").asText();
            String ipAddress = customer.path("ip_address").asText();
            String displayName = customer.path("display_name").asText();
            String fName = customer.path("first_name").asText();
            String lName = customer.path("last_name").asText();
            String email = customer.path("email").asText();
            UpdateCustomerDetails updateCustomerDetails = new UpdateCustomerDetails();
            updateCustomerDetails.setCustomerId(custId);
            updateCustomerDetails.setDisplayName(displayName);
            updateCustomerDetails.setEmail(email);
            updateCustomerDetails.setSubscriptionId(subId);
            updateCustomerDetails.setFirstName(fName);
            updateCustomerDetails.setLastName(lName);
            updateCustomerDetails.setStartDate(startDate);
            updateCustomerDetails.setEndDate(endDate);
            subscriptionsService.updateUser(updateCustomerDetails);


            return restResponse.success("User searched");
        }
        catch (Exception e){

        }
        return restResponse.success("User searched");
    }

    @RequestMapping(value = "/createCollabUser", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<?> createCollabUser(@RequestParam("emailId") String emailId, @RequestParam("organizer") String organizer) {
        try{
            String[] emailIds = emailId.split(",");


            for(String emailIdString : emailIds){
                CreateCustomerDTO createCustomerDTO = new CreateCustomerDTO();
                createCustomerDTO.setCompany_name("Collab-User");
                createCustomerDTO.setEmail(emailIdString);
                createCustomerDTO.setDisplay_name("Collab User - " + (new Date()).getTime());
                createCustomerDTO.setFirst_name("Guest ");
                createCustomerDTO.setLast_name(" User");
                String customerId = subscriptionsService.createUser(createCustomerDTO).toString();
                String subscriptionId = subscriptionsService.createTrialSubscriptionForExistingCustomer(customerId);

                UpdateCustomerDetails updateCustomerDetails = new UpdateCustomerDetails();
                updateCustomerDetails.setCustomerId(customerId);
                updateCustomerDetails.setDisplayName(emailIdString);
                updateCustomerDetails.setEmail(emailIdString);
                updateCustomerDetails.setSubscriptionId(subscriptionId);
                updateCustomerDetails.setFirstName(emailIdString);
                updateCustomerDetails.setLastName(emailIdString);

                Calendar calendar = Calendar.getInstance();
                Date date = calendar.getTime();
                DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
                String strDate = dateFormat.format(date);
                calendar.add(Calendar.DATE, 30);
                Date endDate = calendar.getTime();
                String endDateString = dateFormat.format(endDate);

                updateCustomerDetails.setStartDate(strDate);
                updateCustomerDetails.setEndDate(endDateString);
                Credential credential = new Credential(emailIdString,"Qaz@1111");
                String token = subscriptionsService.updateUser(updateCustomerDetails);
                SendGridEmailService.sendMail(emailIdString,credential, organizer);
                if(null != token){
                    SendGridEmailService.sendTokenMail(emailIdString,token);
                }
            }


            restResponse.success("User Created");
        }
        catch (Exception e){

        }
        return restResponse.success("None");
    }
}

package com.trezi.controllers;

import biweekly.Biweekly;
import biweekly.ICalendar;
import biweekly.component.VEvent;
import biweekly.parameter.CalendarUserType;
import biweekly.parameter.ICalParameters;
import biweekly.parameter.ParticipationLevel;
import biweekly.parameter.ParticipationStatus;
import biweekly.property.*;
import biweekly.util.Duration;
import biweekly.util.Frequency;
import biweekly.util.Recurrence;

import javax.activation.DataHandler;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import java.util.*;

public class SampleJava {

    public static void main(String[] args){
        ICalendar ical = new ICalendar();

        VEvent event = new VEvent();
        Summary summary = event.setSummary("VR Meeting");
        summary.setLanguage("en-us");

        Calendar calendar = Calendar.getInstance();
        calendar.set(2019, 5, 6, 12, 15);
        Date start = calendar.getTime();
        event.setDateStart(start);

        Duration duration = new Duration.Builder().hours(1).build();
        event.setDuration(duration);
        Organizer organizer = new Organizer("vedaprakash.n@smartvizx.com", "vedaprakash.n@smartvizx.com");
        event.setOrganizer(organizer);

        Attendee attendee = new Attendee("vedaprakash.n@smartvizx.com", "vedaprakash.n@smartvizx.com");
        attendee.setParticipationLevel(ParticipationLevel.REQUIRED);
        attendee.setRsvp(true);
        attendee.setEmail("vedaprakash.n@smartvizx.com");
        attendee.setParticipationStatus(ParticipationStatus.NEEDS_ACTION);
        attendee.setCalendarUserType(CalendarUserType.INDIVIDUAL);

        event.setCreated(new Date());
        event.setStatus(Status.confirmed());
        event.addAttendee(attendee);
        Categories categories = new Categories();
        ICalParameters iCalParameters = new ICalParameters();

        categories.setParameters(iCalParameters);
        /*Recurrence recur = new Recurrence.Builder(Frequency.WEEKLY).interval(2).build();
        event.setRecurrenceRule(recur);*/
        ical.addEvent(event);
//        ical.addCategories(categories);

        String str = Biweekly.write(ical).go();
        System.out.println(" the mail string : " + str);
        sendMail(str, "vedaprakash.n@smartvizx.com");
    }

    public static void sendMail(String sb, String emailIds){
        try {

            final String fromEmail = "vedaprakash.n@gmail.com"; //requires valid gmail id
            final String toEmail = "vedaprakash.n@smartvizx.com"; // can be any email id
            String[] toEmails = { "vedaprakash.n@smartvizx.com" };
            System.out.println("TLSEmail Start");
            Properties props = new Properties();
            props.put("mail.smtp.host", "smtp.gmail.com"); //SMTP Host
            props.put("mail.smtp.port", "587"); //TLS Port
            props.put("mail.smtp.auth", "true"); //enable authentication
            props.put("mail.smtp.starttls.enable", "true"); //enable STARTTLS
            props.setProperty("mail.user", "vedaprakash.n@gmail.com");
            props.setProperty("mail.password", "nidumolus");
            Session session = Session.getDefaultInstance(props, null);

            MimeMessage message = new MimeMessage(session);

            // Create the message part
            BodyPart messageBodyPart = new MimeBodyPart();

            // Fill the message
            messageBodyPart.setHeader("Content-Class", "urn:content-classes:calendarmessage");
            messageBodyPart.setHeader("Content-ID", "calendar_message");
            messageBodyPart.setDataHandler(new DataHandler(
                    new ByteArrayDataSource(sb.toString(), "text/calendar")));

            // Set From: header field of the header.
            message.setFrom(new InternetAddress(fromEmail));

            // Set To: header field of the header.
//            for(String toUser : toEmails){
            message.addRecipients(Message.RecipientType.TO, (emailIds));
//            }
            // Set Subject: header field
            message.setSubject("VR Meeting Invite");

            // Send the actual HTML message, as big as you like
//            message.setContent(sb.toString(),"text/html");
            Multipart multipart = new MimeMultipart();

            // Add part one
            multipart.addBodyPart(messageBodyPart);
            message.setContent(multipart);

            // Send message
            Transport transport = session.getTransport("smtp");

            transport.connect("smtp.gmail.com", fromEmail, "nidumolus");
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
            System.out.println("Email Sent");
        }
        catch (Exception e){
            System.out.println("Email Not Sent " + e.getMessage());
        }
    }
}

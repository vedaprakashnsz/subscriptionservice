package com.trezi.services;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.UpdateOptions;
import com.trezi.dto.*;
import com.trezi.utility.Constants;
import org.apache.http.HttpResponse;
import org.bson.BsonDocument;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.springframework.http.ResponseEntity;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class SubscriptionsService {

    @Inject
    UtilityService utilityService;

    public Object getUserDetails(String email){


        return null;
    }

    public Object createUser(CreateCustomerDTO createCustomerDTO){
        try{
            String uri = "https://subscriptions.zoho.com/api/v1/customers";
            createCustomerDTO.setGst_no("123451234512345");
            createCustomerDTO.setGst_treatment("business_gst");
            createCustomerDTO.setStatus("active");
            ResponseEntity response = utilityService.makePOSTAPI(createCustomerDTO, uri);
            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = mapper.readTree(response.getBody().toString());
            JsonNode dataNode = root.path("customer");
            String customerId = dataNode.path("customer_id").asText();
            return customerId;
        }
        catch (Exception e){

        }
        return null;
    }

    public String getProducts(){
        String productId = null;
        try{
            String uri = "https://subscriptions.zoho.com/api/v1/products";
            ResponseEntity response = utilityService.makeGETAPICall(uri, null);
            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = mapper.readTree(response.getBody().toString());
            ArrayNode dataNodes = (ArrayNode) root.path("products");
            for(JsonNode node : dataNodes){
                if(node.path("name").asText().equalsIgnoreCase("Trezi by SmartVizX")){
                    productId = node.path("product_id").asText();
                }
            }

        }
        catch (Exception e){

        }
        return productId;

    }

    public Map getPlanForProduct(String productId){
        Map<String, String> plans = new HashMap<String, String>();
        try {
            String url = "https://subscriptions.zoho.com/api/v1/plans?filter_by=PlanStatus.ACTIVE&product_id=" + productId;
            ResponseEntity response = utilityService.makeGETAPICall(url, null);
            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = mapper.readTree(response.getBody().toString());
            ArrayNode dataNodes = (ArrayNode) root.path("plans");
            for(JsonNode node : dataNodes){
                plans.put(node.path("plan_code").asText(), node.path("name").asText());
            }
        }
        catch (Exception e){

        }
        return plans;
    }

    public String createTrialSubscriptionForExistingCustomer(String customerId){
        try{
            String url = "https://subscriptions.zoho.com/api/v1/customers/" + customerId;
            TrialSubscriptionDTO trialSubscriptionDTO = new TrialSubscriptionDTO();
            ResponseEntity response = utilityService.makeGETAPICall(url, null);
            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = mapper.readTree(response.getBody().toString());
            JsonNode dataNode = root.path("customer");
            String email = dataNode.path("email").asText();
            String displayName = dataNode.path("display_name").asText();
            TrialPlanDTO trialPlanDTO = null;
            CustomerDetailsDTO customerDetailsDTO = new CustomerDetailsDTO();
            customerDetailsDTO.setDisplay_name(displayName);
            customerDetailsDTO.setEmail(email);
            String productId = getProducts();
            Map plans = getPlanForProduct(productId);
            String plan = plans.get(Constants.TRIAL_PLAN).toString();
            if(null != plan){
                trialPlanDTO = new TrialPlanDTO();
                trialPlanDTO.setPlan_code(Constants.TRIAL_PLAN);
            }
            trialSubscriptionDTO.setCustomer(customerDetailsDTO);
            trialSubscriptionDTO.setPlan(trialPlanDTO);
            Calendar calendar = Calendar.getInstance();
            Date date = calendar.getTime();
            DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
            String strDate = dateFormat.format(date);
            trialSubscriptionDTO.setCreated_at(strDate);
            trialSubscriptionDTO.setActivated_at(strDate);
            calendar.add(Calendar.DATE, 30);
            Date endDate = calendar.getTime();
            String endDateString = dateFormat.format(endDate);
            trialSubscriptionDTO.setExpires_at(endDateString);
            trialSubscriptionDTO.setCustomer_id(customerId);

            String jsonInString = new ObjectMapper().writeValueAsString(trialSubscriptionDTO);

            String subscriptionsUrl = "https://subscriptions.zoho.com/api/v1/subscriptions";
            ResponseEntity responseEntity = utilityService.makePOSTAPI(trialSubscriptionDTO, subscriptionsUrl);

            return ((JsonNode)mapper.readTree(responseEntity.getBody().toString())).path("subscription").path("subscription_id").asText();

        }
        catch (Exception e){

        }
        return null;
    }

    public CustomerSubscriptionDetailsDTO fetchCustomerAndSubscriptionDetails(String customerId, String subscriptionId){
        CustomerSubscriptionDetailsDTO customerSubscriptionDetailsDTO = new CustomerSubscriptionDetailsDTO();
        try{
            String customerUrl = "https://subscriptions.zoho.com/api/v1/customers/" + customerId;
            String subscriptionUrl = "https://subscriptions.zoho.com/api/v1/subscriptions/" + subscriptionId;

            ResponseEntity cResponse = utilityService.makeGETAPICall(customerUrl, null);

            ResponseEntity sResponse = utilityService.makeGETAPICall(subscriptionUrl, null);

            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = mapper.readTree(cResponse.getBody().toString());
            JsonNode dataNode = root.path("customer");

            String email = dataNode.path("email").asText();
            String displayName = dataNode.path("display_name").asText();
            String status = dataNode.path("status").asText();
            String firstName = dataNode.path("first_name").asText();
            String lastName = dataNode.path("last_name").asText();

            customerSubscriptionDetailsDTO.setCustomerId(customerId);
            customerSubscriptionDetailsDTO.setDisplayName(displayName);
            customerSubscriptionDetailsDTO.setEmail(email);
            customerSubscriptionDetailsDTO.setFirstName(firstName);
            customerSubscriptionDetailsDTO.setLastName(lastName);
            customerSubscriptionDetailsDTO.setStatus(status);

            root = mapper.readTree(sResponse.getBody().toString());
            dataNode = root.path("subscription");
            String subscriptionType = dataNode.path("status").asText();
            String startDate = dataNode.path("current_term_starts_at").asText();
            String endDate = dataNode.path("current_term_ends_at").asText();


            customerSubscriptionDetailsDTO.setSubscriptionType(subscriptionType);
            customerSubscriptionDetailsDTO.setStartDate(startDate);
            customerSubscriptionDetailsDTO.setEndDate(endDate);
            customerSubscriptionDetailsDTO.setSubscriptionId(subscriptionId);

        }
        catch (Exception e){

        }
        return customerSubscriptionDetailsDTO;
    }

    public String fetchCustomerDetailsByEmail(String email){
        String customerId = null;
        try{
            int i = 1;
            while(customerId == null){
                String customersUrl = "https://subscriptions.zoho.com/api/v1/customers?page="+ i + "&per_page=200";
                ResponseEntity cResponse = utilityService.makeGETAPICall(customersUrl, null);

                ObjectMapper mapper = new ObjectMapper();
                JsonNode root = mapper.readTree(cResponse.getBody().toString());
                ArrayNode dataNodes = (ArrayNode) root.path("customers");
                for(JsonNode node : dataNodes){
                    String emailFromResponse = node.path("email").asText();
//                    System.out.println("the email : " + emailFromResponse);
                    if(emailFromResponse.equalsIgnoreCase(email)){
                        customerId = node.path("customer_id").asText();
                        break;
                    }
                }
                i ++;
            }
        }
        catch (Exception e){

        }
        return customerId;
    }

    public String fetchSubscriptionDetailsByCustomer(String customerId){
        String subscriptionId = null;
        try{
            String subscriptionsUrl = "https://subscriptions.zoho.com/api/v1/subscriptions/?customer_id=" + customerId;
            ResponseEntity cResponse = utilityService.makeGETAPICall(subscriptionsUrl, null);

            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = mapper.readTree(cResponse.getBody().toString());
            ArrayNode dataNodes = (ArrayNode) root.path("subscriptions");
            for(JsonNode node : dataNodes){
                subscriptionId = node.path("subscription_id").asText();
            }
        }
        catch (Exception e){

        }
        return subscriptionId;
    }

    public CustomerSubscriptionDetailsDTO fetchCustomerByEmail(String email){
        CustomerSubscriptionDetailsDTO customerSubscriptionDetailsDTO = null;
        String customerId = fetchCustomerDetailsByEmail(email);
        String subscriptionId = fetchSubscriptionDetailsByCustomer(customerId);
        customerSubscriptionDetailsDTO = fetchCustomerAndSubscriptionDetails(customerId, subscriptionId);
        return customerSubscriptionDetailsDTO;
    }

    public String updateUser(UpdateCustomerDetails updateCustomerDetails){
        try{
            MongoClient mongoClient = MongoClients.create("mongodb://trezidb-prod:CuWOOwzH4llFJ21wXXZh19GgYH3BAPJ4dGWpBMoqytwpkteC8ZeiFs4edv0U0he8zs9hsYMRUfNJjNjj7HqItw==@trezidb-prod.documents.azure.com:10255/trezi-mongo?ssl=true&replicaSet=globaldb");
            MongoDatabase database = mongoClient.getDatabase("trezi-mongo");
            MongoCollection<Document> mongoUsersCollection = database.getCollection("users");
            MongoCollection<Document> mongoSubsCollection = database.getCollection("subscriptions");
            MongoCollection<Document> mongoTokensCollection = database.getCollection("tokens");
            Boolean isPresent = false;
            String token = null;

            for(Document doc : mongoUsersCollection.find()){
                if(doc.get("email").toString().equalsIgnoreCase(updateCustomerDetails.getEmail())){
                    System.out.println(" user present");

                    Document newDoc = new Document(doc);
                    newDoc.put("email", updateCustomerDetails.getEmail());
                    newDoc.put("lName", updateCustomerDetails.getLastName());
                    newDoc.put("fName", updateCustomerDetails.getFirstName());
                    newDoc.put("customerId", updateCustomerDetails.getCustomerId());
//                    BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
//                    String password = "Qaz@1111";
//                    String hashedPassword = bCryptPasswordEncoder.encode(password);
                    String hashedPassword = "$2a$10$fcKgihxTrVbk7ILRv5U.PO0zMrG2YkzcFCSDaYUjaO6Rk8taqI54G";
                    newDoc.put("password", hashedPassword);

                    mongoUsersCollection.deleteOne(doc);
                    mongoUsersCollection.insertOne(newDoc);
                    isPresent = true;
                    break;
                }
            }
            if(isPresent){
                for(Document doc : mongoSubsCollection.find()){
                    if(doc.get("email").toString().equalsIgnoreCase(updateCustomerDetails.getEmail())){
                        System.out.println(" subscription present");

                        Document subsDetail = new Document(doc);
                        subsDetail.put("subscriptionId",updateCustomerDetails.getSubscriptionId());
                        subsDetail.put("email",updateCustomerDetails.getEmail());

                        Date endDate =new SimpleDateFormat("yyyy-MM-dd").parse(updateCustomerDetails.getEndDate());
                        Date startDate =new SimpleDateFormat("yyyy-MM-dd").parse(updateCustomerDetails.getStartDate());
                        subsDetail.put("endDate",endDate.getTime());
                        subsDetail.put("startDate",startDate.getTime());
                        subsDetail.put("licenseType","individual");
                        mongoSubsCollection.deleteOne(doc);
                        mongoSubsCollection.insertOne(subsDetail);
                        break;
                    }
                }
            }

            if(!isPresent){
                Document userDetail = new Document();
                userDetail.put("email", updateCustomerDetails.getEmail());
                userDetail.put("lName", updateCustomerDetails.getLastName());
                userDetail.put("fName", updateCustomerDetails.getFirstName());
                userDetail.put("customerId", updateCustomerDetails.getCustomerId());
                userDetail.put("country","IND");
//                BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
//                String password = "password";
//                String hashedPassword = bCryptPasswordEncoder.encode(password);
////                        Base64.getEncoder().encodeToString(password.getBytes());
////                            bCryptPasswordEncoder.encode(password);
                String hashedPassword = "$2a$10$fcKgihxTrVbk7ILRv5U.PO0zMrG2YkzcFCSDaYUjaO6Rk8taqI54G";
                userDetail.put("password", hashedPassword);
                mongoUsersCollection.insertOne(userDetail);

                Document tokenDetail = new Document();
                tokenDetail.put("email",updateCustomerDetails.getEmail());
                tokenDetail.put("createdAt",(new Date()).getTime());

//                byte[] array = new byte[16];
//                new Random().nextBytes(array);
//                String generatedString = new String(array, Charset.forName("UTF-8"));
                String generatedString = randomAlphaNumeric(16);

                tokenDetail.put("token",generatedString);
                mongoTokensCollection.insertOne(tokenDetail);



                Document subsDetail = new Document();
                subsDetail.put("subscriptionId",updateCustomerDetails.getSubscriptionId());
                subsDetail.put("email",updateCustomerDetails.getEmail());

                Date endDate =new SimpleDateFormat("yyyy-MM-dd").parse(updateCustomerDetails.getEndDate());
                Date startDate =new SimpleDateFormat("yyyy-MM-dd").parse(updateCustomerDetails.getStartDate());
                subsDetail.put("endDate",endDate.getTime());
                subsDetail.put("startDate",startDate.getTime());
                subsDetail.put("licenseType","individual");
                mongoSubsCollection.insertOne(subsDetail);
                token = generatedString;
                return token;
            }
        }
        catch (Exception e){
            System.out.println(" error : " + e.getMessage());
        }
        return null;
    }


    private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    public static String randomAlphaNumeric(int count) {
        StringBuilder builder = new StringBuilder();
        while (count-- != 0) {
            int character = (int)(Math.random()*ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }
        System.out.println(" the token : " + builder.toString());
        return builder.toString();
    }
}

package com.trezi.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.trezi.dto.CreateCustomerDTO;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;

@Service
public class UtilityService {

    @Inject
    RestTemplate restTemplate;

    public ResponseEntity makeGETAPICall(String uri, Map<String,String> parameters){

        try{
            StringBuffer params = new StringBuffer();
            if(null != parameters){
                if(parameters.keySet().size() > 0 ) params.append("?");
                for(String key : parameters.keySet()){
                    params.append(key).append("=").append(parameters.get(key));
                    params.append("&");
                }
                uri = uri + params.toString().substring(0, params.lastIndexOf("&"));
            }

            Map<String, String> headers = new HashMap<String, String>();
            headers.put("authorization", "Zoho-authtoken 20fe704129f88dc5a1c03d6db8abc5e4");
            headers.put("x-com-zoho-subscriptions-organizationid", "668565123");
            headers.put("cookie", "56dd2ec83e=e5a00b03364d3f9066548d8af57881c9; JSESSIONID=0D0311CBC11D2FBA0CE7F15D051D4B44; zsmcscookocrmai=ad13a0f6-d9af-422c-a37b-85590c764941; zsmcscook=aa77b15895cddae8575269589b07b0528fc6cfce2dafa2b0f309cfa98bd6e80be9ea231a27d8ef2220dfa1215baca0dcb9b996d408dd389f1d037c1719fb17cc; _zxor=\"https://accounts.zoho.com\"; 56dd2ec83e=e5a00b03364d3f9066548d8af57881c9; JSESSIONID=0D0311CBC11D2FBA0CE7F15D051D4B44; zsmcscookocrmai=ad13a0f6-d9af-422c-a37b-85590c764941; zsmcscook=aa77b15895cddae8575269589b07b0528fc6cfce2dafa2b0f309cfa98bd6e80be9ea231a27d8ef2220dfa1215baca0dcb9b996d408dd389f1d037c1719fb17cc; _zxor=\"https://accounts.zoho.com\"; JSESSIONID=1359A773D412375ABCD6B2BCC0724E20");

            HttpHeaders httpHeaders = createHttpHeaders(headers);
            HttpEntity entity = new HttpEntity<>(httpHeaders);
            ResponseEntity response = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
            return response;
        }
        catch (Exception e){
            e.getMessage();
        }
        return null;
    }


    public ResponseEntity makePOSTAPI(Object object, String url){
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("authorization", "Zoho-authtoken 20fe704129f88dc5a1c03d6db8abc5e4");
        headers.put("x-com-zoho-subscriptions-organizationid", "668565123");
        headers.put("cookie", "56dd2ec83e=e5a00b03364d3f9066548d8af57881c9; JSESSIONID=0D0311CBC11D2FBA0CE7F15D051D4B44; zsmcscookocrmai=ad13a0f6-d9af-422c-a37b-85590c764941; zsmcscook=aa77b15895cddae8575269589b07b0528fc6cfce2dafa2b0f309cfa98bd6e80be9ea231a27d8ef2220dfa1215baca0dcb9b996d408dd389f1d037c1719fb17cc; _zxor=\"https://accounts.zoho.com\"; 56dd2ec83e=e5a00b03364d3f9066548d8af57881c9; JSESSIONID=0D0311CBC11D2FBA0CE7F15D051D4B44; zsmcscookocrmai=ad13a0f6-d9af-422c-a37b-85590c764941; zsmcscook=aa77b15895cddae8575269589b07b0528fc6cfce2dafa2b0f309cfa98bd6e80be9ea231a27d8ef2220dfa1215baca0dcb9b996d408dd389f1d037c1719fb17cc; _zxor=\"https://accounts.zoho.com\"; JSESSIONID=1359A773D412375ABCD6B2BCC0724E20");


        ObjectMapper objectMapper = new ObjectMapper();
        Map json = objectMapper.convertValue(object, Map.class);


        HttpHeaders httpHeaders = createHttpHeaders(headers);
        HttpEntity<String> entity = null;
        try {
            entity = new HttpEntity<String>(new ObjectMapper().writeValueAsString(json), httpHeaders);
            ResponseEntity response = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
            return response;
        }
        catch(Exception e){
            e.getMessage();
        }
        return null;
    }


    private HttpHeaders createHttpHeaders(Map<String,String> headerValues)
    {
        HttpHeaders headers = new HttpHeaders();
        for(String s : headerValues.keySet()){
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.add(s, headerValues.get(s));
        }
        return headers;
    }
}

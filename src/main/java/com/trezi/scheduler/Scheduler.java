package com.trezi.scheduler;

import com.trezi.utility.SendStartMail;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
public class Scheduler {

    @Scheduled(cron = "0 0 6 * * ?")
    public void scheduleTaskWithFixedDelay() {
        try {
            System.out.println("inside scheduler");
            TimeUnit.SECONDS.sleep(5);
            SendStartMail.captureData("vedaprakash.n@smartvizx.com,ganesh.r@smartvizx.com,praveen.m@smartvizx.com");
        } catch (InterruptedException ex) {
            throw new IllegalStateException(ex);
        }
    }
}
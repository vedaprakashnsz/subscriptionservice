package com.trezi.utility;

import java.util.Date;

public class CustomerInfo {
    String email;
    Date endDate;
    String subType;

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String toString(){
        return "Email : " + this.getEmail() + " , End Date : " + this.getEndDate() + "\n";
    }
}

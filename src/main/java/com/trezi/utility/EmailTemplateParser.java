package com.trezi.utility;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.util.ResourceUtils;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import java.io.File;
import java.io.FileNotFoundException;
import java.time.Instant;
import java.util.*;

public class EmailTemplateParser {

    public static void sendAccountMail(String emailIds){
        try {

            final String fromEmail = "azure_78a62f6a53fb8279454be82f361ad2a8@azure.com"; //requires valid gmail id
            //final String toEmail = "vedaprakash.n@smartvizx.com"; // can be any email id
            //String[] toEmails = { "vedaprakash.n@smartvizx.com" };
            Properties props = new Properties();
            props.put("mail.smtp.host", "smtp.sendgrid.net"); //SMTP Host
            props.put("mail.smtp.port", "587"); //TLS Port
            props.put("mail.smtp.auth", "true"); //enable authentication
            props.put("mail.smtp.starttls.enable", "true"); //enable STARTTLS
            props.setProperty("mail.user", "azure_78a62f6a53fb8279454be82f361ad2a8@azure.com");
            props.setProperty("mail.password", "Smartvizx#123");
            Session session = Session.getDefaultInstance(props, null);
            // Create the message part
//            BodyPart messageBodyPart = new MimeBodyPart();
//
//            // Fill the message
//            messageBodyPart.setHeader("Content-Class", "urn:content-classes:calendarmessage");
//            messageBodyPart.setHeader("Content-ID", "calendar_message");
//            messageBodyPart.setDataHandler(new DataHandler(
//                    new ByteArrayDataSource(sb.toString(), "text/calendar;method=REQUEST")));
            List<BodyPart> imageParts = constructImageParts();

            // Send message
            Transport transport = session.getTransport("smtp");
            Address[] addresses = InternetAddress.parse(emailIds);

            transport.connect("smtp.sendgrid.net", fromEmail, "Smartvizx#123");

            File emailTemplate = ResourceUtils.getFile("classpath:EmailInvite.html");
            for(Address address:addresses){

                MimeMessage mimeMessage = new MimeMessage(session);

                mimeMessage.addHeaderLine("method=REQUEST");
                mimeMessage.addHeaderLine("charset=UTF-8");
                mimeMessage.setFrom(new InternetAddress("donotreply@smartvizx.com"));
                mimeMessage.addRecipients(Message.RecipientType.TO, (emailIds));
                mimeMessage.setSubject("null");

                //Add Html body part (template)
                BodyPart messageBodyPartHtml = new MimeBodyPart();

                //Dynamic substitution
                Document document = org.jsoup.Jsoup.parse(emailTemplate,"UTF-8" );
                processDynamicSubstitution(document, emailIds );

                messageBodyPartHtml.setContent( document.toString(), "text/html");

                Multipart multipart = new MimeMultipart();

                for (BodyPart imagePart : imageParts) {
                    multipart.addBodyPart(imagePart);
                }
//                multipart.addBodyPart(messageBodyPart);
                multipart.addBodyPart(messageBodyPartHtml);

                mimeMessage.setContent(multipart);

                transport.sendMessage(mimeMessage, new Address[]{address});
            }
            transport.close();
        }
        catch (Exception e){
        }
    }

    private static void processDynamicSubstitution(Document document, String emailIds) {
        HashMap<String, String> dynamicSubstitutionMap = new HashMap<>();
        dynamicSubstitutionMap.put("InviteeName", "mourya");
        dynamicSubstitutionMap.put("UserName", "mouryaUser");



        dynamicSubstitutionMap.entrySet().forEach(entry -> {
                    Elements e2 = document.getElementsMatchingOwnText("\\$"+entry.getKey()+"\\$");
                    e2.forEach(element -> {
                        String str = element.text();
                        element.text(str.replaceAll("\\$"+entry.getKey()+"\\$" , entry.getValue()));
                    });
                }
        );

        //Add attendees to the html body - (exclude organizer in the list )
        Element attendeeListElement = document.getElementById("attendeeList");
        String[] emails = emailIds.split(",");
        for(String email : emails){
//            if(!(email.equals(meeting.getCreatedBy()))){ //Exclude Organizer
                attendeeListElement.append("<li style=\"line-height: 18px; font-size: 12px;\"><span style=\"font-size: 14px; color: white; line-height: 21px;\">"+ "<a style=\"text-decoration:none; color:white\">"+email+"</a>" +"</span></li>");
//            }
        }
    }

    private static List<BodyPart> constructImageParts() throws FileNotFoundException, MessagingException {

        List<BodyPart> bodyParts = new ArrayList<>();

        File[] imageResources = new File[4];
        imageResources[0] = ResourceUtils.getFile("classpath:images/TreziOnlyDarkNoBG.png");
        imageResources[1] = ResourceUtils.getFile("classpath:images/download_2.png");
        imageResources[2] = ResourceUtils.getFile("classpath:images/meetingProject.png");
        imageResources[3] = ResourceUtils.getFile("classpath:images/joinMeeting_1.png");

        for(int i=0;i<4;i++){
            MimeBodyPart messageBodyPartImage = new MimeBodyPart();
            messageBodyPartImage.setDataHandler(new DataHandler(new FileDataSource(imageResources[i])));
            messageBodyPartImage.setContentID("<image"+i+">");
            messageBodyPartImage.setDisposition(MimeBodyPart.INLINE);
            bodyParts.add(messageBodyPartImage);
        }
        return bodyParts;

    }
}

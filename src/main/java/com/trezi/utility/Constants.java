package com.trezi.utility;

public class Constants {

    public static final String RESPONSE_SUCCESS_STATUS = "success";
    public static final String RESPONSE_FAILED_STATUS = "failure";
    public static final Integer REQUEST_FAILURE_CODE = 500;
    public static final Integer REQUEST_SUCCESS_CODE = 200;
    public static final Integer UNAUTHORIZED_ACCESS_CODE = 401;
    public static final String OWNED = "owned";
    public static final String SHARED = "shared";
    public static final String TRIAL_PLAN = "TZI-DFT";
    public static final String SUBSCRIPTION_EMAIL_SUBJECT = "Account information for Trezi collaborative meeting invitation";
    public static final String TOKEN_EMAIL_SUBJECT = "Verification mail from Trezi";
}
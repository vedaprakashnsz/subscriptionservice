package com.trezi.utility;

import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.*;

public class SendMail {

    public static void main(String[] args) {
        MongoClient mongoClient = MongoClients.create("mongodb://trezidb-prod:CuWOOwzH4llFJ21wXXZh19GgYH3BAPJ4dGWpBMoqytwpkteC8ZeiFs4edv0U0he8zs9hsYMRUfNJjNjj7HqItw==@trezidb-prod.documents.azure.com:10255/trezi-mongo?ssl=true&replicaSet=globaldb");
        Long presentDay = (new Date()).getTime();
        Calendar calendar = Calendar.getInstance();
        Date now = calendar.getTime();
        System.out.println(" present talent : " + calendar.getTime().getTime());
        calendar.add(Calendar.DATE, 7);
        Date oneWeek = calendar.getTime();
        System.out.println(" one week : " + calendar.getTime().getTime());
        calendar.add(Calendar.MONTH, 1);
        Date oneMonth = calendar.getTime();
        System.out.println(" one month : " + calendar.getTime().getTime());
        calendar.add(Calendar.MONTH, 3);
        Date threeMonths = calendar.getTime();
        MongoDatabase database = mongoClient.getDatabase("trezi-mongo");
        MongoCollection<Document> mongoCollection = database.getCollection("subscriptions");
        List<CustomerInfo> weekCustomers = new ArrayList<CustomerInfo>();
        List<CustomerInfo> monthCustomers = new ArrayList<CustomerInfo>();
        List<CustomerInfo> threeMonthCustomers = new ArrayList<CustomerInfo>();
        Integer trialWeekly = new Integer(0);
        Integer trialMonthly = 0;
        Integer trialThreeMonths = 0;
        Integer monthWeekly = 0;
        Integer monthMonthly = 0;
        Integer monthhreeMonths = new Integer(0);
        Integer yearlyWeekly = 0;
        Integer yearlyMonthly = 0;
        Integer yearlyThreeMonths = new Integer(0);
        Map<String,List<CustomerInfo>> customerMap = new HashMap<String, List<CustomerInfo>>();
        for (Document doc : mongoCollection.find().sort(new BasicDBObject("endDate", 1))) {
            if(doc.get("endDate") != null){
                if(now.getTime() < Double.parseDouble(doc.get("endDate").toString()) && oneWeek.getTime() > Double.parseDouble(doc.get("endDate").toString())){
                    Date d = null;
                    try{
                        d = new Date(doc.getDouble("endDate").longValue());
                    }
                    catch (ClassCastException e){
                        d = new Date(doc.getLong("endDate").longValue());
                    }
                    System.out.println("the email : " + doc.get("email") + "Week Date : " + d) ;
                    CustomerInfo customerInfo = new CustomerInfo();
                    customerInfo.setEmail(doc.get("email").toString());
                    customerInfo.setEndDate(d);
                    customerInfo.setSubType(doc.get("subPlan").toString());
                    if(doc.get("subPlan").toString().equalsIgnoreCase("trial")){
                        trialWeekly ++;
                    }
                    else if(doc.get("subPlan").toString().equalsIgnoreCase("monthly")){
                        monthWeekly ++;
                    }
                    else if(doc.get("subPlan").toString().equalsIgnoreCase("yearly")){
                        yearlyWeekly ++;
                    }
                    weekCustomers.add(customerInfo);
                }
                else if(oneWeek.getTime() < Double.parseDouble(doc.get("endDate").toString()) && oneMonth.getTime() > Double.parseDouble(doc.get("endDate").toString())){
                    Date d = null;
                    try{
                        d = new Date(doc.getDouble("endDate").longValue());
                    }
                    catch (ClassCastException e){
                        d = new Date(doc.getLong("endDate").longValue());
                    }
                    System.out.println("the email : " + doc.get("email") + "Month Date : " + d) ;
                    CustomerInfo customerInfo = new CustomerInfo();
                    customerInfo.setEmail(doc.get("email").toString());
                    customerInfo.setEndDate(d);
                    customerInfo.setSubType(doc.get("subPlan").toString());
                    if(doc.get("subPlan").toString().equalsIgnoreCase("trial")){
                        trialMonthly ++;
                    }
                    else if(doc.get("subPlan").toString().equalsIgnoreCase("monthly")){
                        monthMonthly ++;
                    }
                    else if(doc.get("subPlan").toString().equalsIgnoreCase("yearly")){
                        yearlyMonthly ++;
                    }
                    monthCustomers.add(customerInfo);
                }
                else if(oneMonth.getTime() < Double.parseDouble(doc.get("endDate").toString()) && threeMonths.getTime() > Double.parseDouble(doc.get("endDate").toString())){
                    Date d = null;
                    try{
                        d = new Date(doc.getDouble("endDate").longValue());
                    }
                    catch (ClassCastException e){
                        d = new Date(doc.getLong("endDate").longValue());
                    }
                    System.out.println("the email : " + doc.get("email") + "Month Date : " + d) ;
                    CustomerInfo customerInfo = new CustomerInfo();
                    customerInfo.setEmail(doc.get("email").toString());
                    customerInfo.setEndDate(d);
                    customerInfo.setSubType(doc.get("subPlan").toString());
                    if(doc.get("subPlan").toString().equalsIgnoreCase("trial")){
                        trialThreeMonths ++;
                    }
                    else if(doc.get("subPlan").toString().equalsIgnoreCase("monthly")){
                        monthhreeMonths ++;
                    }
                    else if(doc.get("subPlan").toString().equalsIgnoreCase("yearly")){
                        yearlyThreeMonths ++;
                    }
                    threeMonthCustomers.add(customerInfo);
                }
            }
        }
        customerMap.put("week",weekCustomers);
        customerMap.put("month",monthCustomers);
        customerMap.put("threeMonths",threeMonthCustomers);
        StringBuffer sb = new StringBuffer();
//        sb.append("Customers whose end date is within a week \n");
//        sb.append("----------------------------------------------------\n");
//        sb.append(customerMap.get("week"));
//        sb.append("\n");
//        sb.append("--------------------------------------------------------\n\n");
//        sb.append("Customers whose end date is within a month \n");
//        sb.append("----------------------------------------------------\n");
//        sb.append(customerMap.get("month"));

        sb.append("<tr>");
        sb.append("Upcoming Renewal Summary \n");
        sb.append("</tr>");
        sb.append("<tr>");
        sb.append("</tr>");

        sb.append("<html><body>"
                + "<table style='border:2px solid black'>");
        sb.append("<tr>");
        sb.append("<td>");
        sb.append("Subscription Type");
        sb.append("</td>");
        sb.append("<td>");
        sb.append("# due in a week");
        sb.append("</td>");
        sb.append("<td>");
        sb.append("# due in a month");
        sb.append("</td>");
        sb.append("<td>");
        sb.append("# due in three months");
        sb.append("</td>");
        sb.append("</tr>");

        sb.append("<tr>");
        sb.append("<td>");
        sb.append("Trial");
        sb.append("</td>");
        sb.append("<td>");
        sb.append(trialWeekly);
        sb.append("</td>");
        sb.append("<td>");
        sb.append(trialMonthly);
        sb.append("</td>");
        sb.append("<td>");
        sb.append(trialThreeMonths);
        sb.append("</td>");
        sb.append("</tr>");

        sb.append("<tr>");
        sb.append("<td>");
        sb.append("Monthly");
        sb.append("</td>");
        sb.append("<td>");
        sb.append(monthWeekly);
        sb.append("</td>");
        sb.append("<td>");
        sb.append(monthMonthly);
        sb.append("</td>");
        sb.append("<td>");
        sb.append(monthhreeMonths);
        sb.append("</td>");
        sb.append("</tr>");

        sb.append("<tr>");
        sb.append("<td>");
        sb.append("Yearly");
        sb.append("</td>");
        sb.append("<td>");
        sb.append(yearlyWeekly);
        sb.append("</td>");
        sb.append("<td>");
        sb.append(yearlyMonthly);
        sb.append("</td>");
        sb.append("<td>");
        sb.append(yearlyThreeMonths);
        sb.append("</td>");
        sb.append("</tr>");

        sb.append("</table></body></html>");

        sb.append("\n");
        sb.append("\n");
        sb.append("<tr>");
        sb.append("</tr>");
        sb.append("<tr>");
        sb.append("Customers whose renewal is due in a week \n");
        sb.append("</tr>");
        sb.append("<tr>");
        sb.append("</tr>");
        sb.append("<html><body>"
                + "<table style='border:2px solid black'>");
        for(CustomerInfo customerInfo : weekCustomers){
            sb.append("<tr>");
            sb.append("<td>");
            sb.append(customerInfo.getEmail());
            sb.append("</td>");
            sb.append("<td>");
            sb.append(customerInfo.getEndDate());
            sb.append("</td>");
            sb.append("<td>");
            sb.append(customerInfo.getSubType());
            sb.append("</td>");
            sb.append("</tr>");
        }
        sb.append("</table></body></html>");


        sb.append("\n");
        sb.append("\n");
        sb.append("<tr>");
        sb.append("</tr>");
        sb.append("<tr>");
        sb.append("Customers whose renewal is due in a month \n");
        sb.append("</tr>");
        sb.append("<tr>");
        sb.append("</tr>");
        sb.append("<html><body>"
                + "<table style='border:2px solid black'>");
        for(CustomerInfo customerInfo : monthCustomers){
            sb.append("<tr>");
            sb.append("<td>");
            sb.append(customerInfo.getEmail());
            sb.append("</td>");
            sb.append("<td>");
            sb.append(customerInfo.getEndDate());
            sb.append("</td>");
            sb.append("<td>");
            sb.append(customerInfo.getSubType());
            sb.append("</td>");
            sb.append("</tr>");
        }
        sb.append("</table></body></html>");

        sb.append("\n");
        sb.append("\n");
        sb.append("<tr>");
        sb.append("</tr>");
        sb.append("<tr>");
        sb.append("Customers whose renewal is due in three months \n");
        sb.append("</tr>");
        sb.append("<tr>");
        sb.append("</tr>");
        sb.append("<html><body>"
                + "<table style='border:2px solid black'>");
        for(CustomerInfo customerInfo : threeMonthCustomers){
            sb.append("<tr>");
            sb.append("<td>");
            sb.append(customerInfo.getEmail());
            sb.append("</td>");
            sb.append("<td>");
            sb.append(customerInfo.getEndDate());
            sb.append("</td>");
            sb.append("<td>");
            sb.append(customerInfo.getSubType());
            sb.append("</td>");
            sb.append("</tr>");
        }
        sb.append("</table></body></html>");

        sendMail(sb);

    }

    public static void sendMail(StringBuffer sb){
        try {

            final String fromEmail = "vedaprakash.n@gmail.com"; //requires valid gmail id
//            final String password = "mypassword"; // correct password for gmail id
            final String toEmail = "vedaprakash.n@smartvizx.com"; // can be any email id
            String[] toEmails = { "vedaprakash.n@smartvizx.com" };
            System.out.println("TLSEmail Start");
            Properties props = new Properties();
            props.put("mail.smtp.host", "smtp.gmail.com"); //SMTP Host
            props.put("mail.smtp.port", "587"); //TLS Port
            props.put("mail.smtp.auth", "true"); //enable authentication
            props.put("mail.smtp.starttls.enable", "true"); //enable STARTTLS
            props.setProperty("mail.user", "vedaprakash.n@smartvizx.com");
            props.setProperty("mail.password", "Qaz@1111    ");
            Session session = Session.getDefaultInstance(props, null);

            MimeMessage message = new MimeMessage(session);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress(fromEmail));

            // Set To: header field of the header.
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmail));

            // Set Subject: header field
            message.setSubject("Trezi : Upcoming Renewals");

            // Send the actual HTML message, as big as you like
            message.setContent(sb.toString(),"text/html");

            // Send message
            Transport transport = session.getTransport("smtp");

            transport.connect("smtp.gmail.com", fromEmail, "nidumolus");
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
            System.out.println("Email Sent");
        }
        catch (Exception e){
            System.out.println("Email Not Sent " + e.getMessage());
        }
    }
}

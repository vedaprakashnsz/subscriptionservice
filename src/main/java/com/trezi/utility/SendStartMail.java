package com.trezi.utility;

import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.text.SimpleDateFormat;
import java.util.*;

public class SendStartMail {

    public static void main(String[] args) {

        captureData(null);
    }

    public static void captureData(String emailIds){
        List<Map> userDetails = new ArrayList<Map>();
        MongoClient mongoClient = MongoClients.create("mongodb://trezidb-prod:CuWOOwzH4llFJ21wXXZh19GgYH3BAPJ4dGWpBMoqytwpkteC8ZeiFs4edv0U0he8zs9hsYMRUfNJjNjj7HqItw==@trezidb-prod.documents.azure.com:10255/trezi-mongo?ssl=true&replicaSet=globaldb");
        List<CustomerInfo> trialCustomers = new ArrayList<CustomerInfo>();
        Long presentDay = (new Date()).getTime();
        Calendar calendar = Calendar.getInstance();

        Date now = calendar.getTime();
        System.out.println(" present talent : " + calendar.getTime().getTime());

//        calendar.clear();
        calendar.add(Calendar.DATE, -2);;
//        Date date1 = null;
//        try{
//            String sDate1="26/02/2019";
//            date1 = new SimpleDateFormat("dd/MM/yyyy").parse(sDate1);
//        }
//        catch (Exception e){
//
//        }

        Date twoDaysBack = calendar.getTime();
        System.out.println(" one week : " + calendar.getTime());


        MongoDatabase database = mongoClient.getDatabase("trezi-mongo");
        MongoCollection<Document> mongoCollection = database.getCollection("subscriptions");

        MongoCollection<Document> mongoUsers = database.getCollection("users");

        Map<String, List<CustomerInfo>> customerMap = new HashMap<String, List<CustomerInfo>>();
        for (Document doc : mongoCollection.find().sort(new BasicDBObject("startDate", 1))) {
            if (doc.get("startDate") != null) {
                if (twoDaysBack.getTime() < Double.parseDouble(doc.get("startDate").toString()) && now.getTime() > Double.parseDouble(doc.get("startDate").toString()) && doc.get("subPlan").toString().equalsIgnoreCase("trial")) {
                    Date d = null;
                    try {
                        d = new Date(doc.getDouble("startDate").longValue());
                    } catch (ClassCastException e) {
                        d = new Date(doc.getLong("startDate").longValue());
                    }
                    System.out.println("the email : " + doc.get("email") + "Week Date : " + d);


                    for(Document user : mongoUsers.find()){
                        if(user.get("email") != null && user.get("email").toString().equalsIgnoreCase(doc.get("email").toString())){
                            System.out.println("the user : " + user);
                            Map usersMap = new LinkedHashMap();
                            usersMap.put("email",user.get("email").toString());
                            usersMap.put("addressLine1", user.get("addressLine1"));
                            usersMap.put("addressLine2", user.get("addressLine1"));
                            usersMap.put("city", user.get("city"));
                            usersMap.put("state", user.get("usrState"));
                            usersMap.put("zip", user.get("zip"));
                            usersMap.put("country", user.get("country"));
                            usersMap.put("fName", user.get("fName"));
                            usersMap.put("lName", user.get("lName"));
                            usersMap.put("org", user.get("org"));
                            userDetails.add(usersMap);
                        }
                    }
                    CustomerInfo customerInfo = new CustomerInfo();
                    customerInfo.setEmail(doc.get("email").toString());
                    customerInfo.setEndDate(d);
                    customerInfo.setSubType(doc.get("subPlan").toString());
                    trialCustomers.add(customerInfo);
                }
            }
        }
        StringBuffer sb = new StringBuffer();

        sb.append("\n");
        sb.append("<tr>");
        sb.append("</tr>");
        sb.append("<tr>");
        sb.append("Trial Sign Ups  \n");
        sb.append("</tr>");
        sb.append("<tr>");
        sb.append("Total : " + trialCustomers.size());
        sb.append("</tr>");
        sb.append("<tr>");
        sb.append("</tr>");
        sb.append("<html><body>"
                + "<table style='border:2px solid black'>");
        for(CustomerInfo customerInfo : trialCustomers){
            sb.append("<tr>");
            sb.append("<td>");
            sb.append(customerInfo.getEmail());
            sb.append("</td>");
            sb.append("<td>");
            sb.append(customerInfo.getEndDate());
            sb.append("</td>");
            sb.append("<td>");
            sb.append(customerInfo.getSubType());
            sb.append("</td>");
            sb.append("</tr>");
        }
        sb.append("</table></body></html>");

        sb.append("<html><body>"
                + "<table style='border:2px solid black'>");
        if(userDetails.size() > 0){
            Map<String,Object> oneUsr = userDetails.get(0);
            sb.append("<tr>");
            for(String key : oneUsr.keySet()){
                sb.append("<td>");
                sb.append(key);
                sb.append("</td>");
            }
            sb.append("</tr>");
            for(Map<String,Object> user : userDetails){
                sb.append("<tr>");
                for(String key : user.keySet()){
                    sb.append("<td>");
                    sb.append(user.get(key));
                    sb.append("</td>");
                }
                sb.append("</tr>");

            }
        }

        sb.append("</table></body></html>");
        sendMail(sb, emailIds);
    }

    public static void sendMail(StringBuffer sb, String emailIds){
        try {

            final String fromEmail = "azure_78a62f6a53fb8279454be82f361ad2a8@azure.com"; //requires valid gmail id
//            final String password = "mypassword"; // correct password for gmail id
            final String toEmail = emailIds; // can be any email id
            String[] toEmails = emailIds.split(",");
            System.out.println("TLSEmail Start");
            Properties props = new Properties();
            props.put("mail.smtp.host", "smtp.sendgrid.net"); //SMTP Host
            props.put("mail.smtp.port", "587"); //TLS Port
            props.put("mail.smtp.auth", "true"); //enable authentication
            props.put("mail.smtp.starttls.enable", "true"); //enable STARTTLS
            props.setProperty("mail.user", "azure_78a62f6a53fb8279454be82f361ad2a8@azure.com");
            props.setProperty("mail.password", "Smartvizx#123");
            Session session = Session.getDefaultInstance(props, null);

            MimeMessage message = new MimeMessage(session);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress(fromEmail));

            // Set To: header field of the header.
//            for(String toUser : toEmails){
                message.addRecipients(Message.RecipientType.TO, (emailIds));
//            }
            // Set Subject: header field
            message.setSubject("Trezi : Trial Sign Ups");

            // Send the actual HTML message, as big as you like
            message.setContent(sb.toString(),"text/html");

            // Send message
            Transport transport = session.getTransport("smtp");

            transport.connect("smtp.sendgrid.net", fromEmail, "Smartvizx#123");
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
            System.out.println("Email Sent");
        }
        catch (Exception e){
            System.out.println("Email Not Sent " + e.getMessage());
        }
    }
}
